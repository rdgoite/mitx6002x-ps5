# 6.00.2x Problem Set 5
# Graph optimization
#
# A set of data structures to represent graphs
#

class Node(object):
    def __init__(self, name):
        self.name = str(name)
    def getName(self):
        return self.name
    def __str__(self):
        return self.name
    def __repr__(self):
        return self.name
    def __eq__(self, other):
        return self.name == other.name
    def __ne__(self, other):
        return not self.__eq__(other)
    def __hash__(self):
        # Override the default hash method
        # Think: Why would we want to do this?
        return self.name.__hash__()

class Edge(object):
    def __init__(self, src, dest):
        self.src = src
        self.dest = dest
    def getSource(self):
        return self.src
    def getDestination(self):
        return self.dest
    def __str__(self):
        return '{0}->{1}'.format(self.src, self.dest)

class WeightedEdge(Edge):
    def __init__(self, source, destination, totalDistance, outdoorDistance):
        Edge.__init__(self, source, destination)
        self.totalDistance = totalDistance
        self.outdoorDistance = outdoorDistance

    def getTotalDistance(self):
        return self.totalDistance

    def getOutdoorDistance(self):
        return self.outdoorDistance
        
    def __str__(self):
        return '{0} ({1}, {2})'.format(Edge.__str__(self), self.totalDistance, self.outdoorDistance)
        
class Digraph(object):
    """
    A directed graph
    """
    def __init__(self):
        # A Python Set is basically a list that doesn't allow duplicates.
        # Entries into a set must be hashable (where have we seen this before?)
        # Because it is backed by a hashtable, lookups are O(1) as opposed to the O(n) of a list (nifty!)
        # See http://docs.python.org/2/library/stdtypes.html#set-types-set-frozenset
        self.nodes = set([])
        self.edges = {}
    def addNode(self, node):
        if node in self.nodes:
            # Even though self.nodes is a Set, we want to do this to make sure we
            # don't add a duplicate entry for the same node in the self.edges list.
            raise ValueError('Duplicate node')
        else:
            self.nodes.add(node)
            self.edges[node] = []
    def addEdge(self, edge):
        src = edge.getSource()
        dest = edge.getDestination()
        if not(src in self.nodes and dest in self.nodes):
            raise ValueError('Node not in graph')
        self.edges[src].append(dest)
    def childrenOf(self, node):
        return self.edges[node]
    def hasNode(self, node):
        return node in self.nodes
    def __str__(self):
        res = ''
        for k in self.edges:
            for d in self.edges[str(k)]:
                res = '{0}{1}->{2}\n'.format(res, k, d)
        return res[:-1]

class WeightedPath(object):
    def __init__(self, path=[], distance=0, outerDistance=0):
        self.path = path
        self.distance = distance
        self.outerDistance = outerDistance

    def link(self, edge):
        if edge.src not in self.path: self.path.append(edge.src)
        if edge.dest not in self.path: self.path.append(edge.dest)
        self.distance += edge.totalDistance
        self.outerDistance += edge.outdoorDistance

    def fork(self, edge):
        forkedPath = WeightedPath([node for node in self.path], self.distance, self.outerDistance)
        forkedPath.link(edge)
        return forkedPath

    def __str__(self):
        return '->'.join([str(node) for node in self.path])
        
class WeightedDigraph(Digraph):
    def __init__(self):
        Digraph.__init__(self)

    def addEdge(self, edge):
        if (WeightedEdge == type(edge)):
            if (edge.src in self.nodes and edge.dest in self.nodes):
                self.edges[edge.src].append(edge)
            else:
                raise ValueError('Node not in graph')
        else:
            raise ValueError('Edge not weighted')

    def childrenOf(self, node):
        return [edge.dest for edge in self.edges[node]]

    def findNode(self, name):
        results = filter(lambda node: name == node.name, self.nodes)
        return results[0] if len(results) > 0 else None

    def findPaths(self, start, end, maximumDistance):
        paths = []
        stack = []
        stack.append(WeightedPath([start]))
        while len(stack) > 0:
            currentPath = stack.pop()
            lastNode = currentPath.path[-1]
            for edge in self.edges[lastNode]:
                node = edge.dest
                if node not in currentPath.path:
                    path = currentPath.fork(edge)
                    if node == end and path.distance <= maximumDistance:
                        paths.append(path)
                    else:
                        stack.append(path)
        return paths
        
    def __str__(self):
        edgeStrings = []
        for node, children in self.edges.items():
            for child in children:
                edgeString = '{0}->{1} ({2}.0, {3}.0)'.format(node, child.dest, child.totalDistance, child.outdoorDistance)
                edgeStrings.append(edgeString)
        return '\n'.join(edgeStrings)
    
