#!/usr/bin/python

from graph import *
from ps5 import *
from unittest import main, skip, TestCase

class WeightedEdgeTest(TestCase):

    def testPrint(self):
        #given:
        nodeA = Node('A')
        nodeB = Node('B')
        edge = WeightedEdge(nodeA, nodeB, 100, 25)

        #expect:
        self.assertEquals('A->B (100, 25)', str(edge))

class WeightedDigraphTest(TestCase):

    def testAddEdge(self):
        #given:
        graph = WeightedDigraph()

        #and:
        nodeA = Node('A')
        nodeB = Node('B')
        for node in [nodeA, nodeB]: graph.addNode(node)

        #and:
        edge = WeightedEdge(nodeA, nodeB, 15, 3)
        graph.addEdge(edge)

        #expect:
        self.assertEquals(2, len(graph.edges))
        self.assertEquals(1, len(graph.edges[nodeA]))

        #and:
        self.assertTrue(edge in graph.edges[nodeA])

    def testAddEdgeWithNonExistentNodes(self):
        #given:
        graph = WeightedDigraph()

        #and:
        node = Node('A')
        graph.addNode(node)

        #and:
        edge = WeightedEdge(node, Node('B'), 10, 3)

        #expect:
        with self.assertRaises(ValueError):
            graph.addEdge(edge)

    def testAddNonWeightedEdge(self):
        #given:
        graph = WeightedDigraph()

        #and:
        nodeA = Node('A')
        nodeB = Node('B')
        for node in [nodeA, nodeB]: graph.addNode(node)

        #and:
        edge = Edge(nodeA, nodeB)

        #expect:
        with self.assertRaises(ValueError):
            graph.addEdge(edge)

    def testGetChildrenOfNode(self):
        #given:
        graph = WeightedDigraph()

        #and:
        nodeA = Node('A')
        nodeB = Node('B')
        nodeC = Node('C')
        for node in [nodeA, nodeB, nodeC]: graph.addNode(node)

        #and:
        data = [(nodeA, nodeB), (nodeA, nodeC), (nodeB, nodeC)]
        for source, destination in data:
            edge = WeightedEdge(source, destination, 1, 1)
            graph.addEdge(edge)

        #expect:
        for node in [nodeB, nodeC]:
            self.assertTrue(node in graph.childrenOf(nodeA))

    def testFindNode(self):
        #given:
        graph = WeightedDigraph()

        #and:
        nodeA = Node('A')
        nodeB = Node('B')
        for node in [nodeA, nodeB]: graph.addNode(node)

        #when:
        resultA = graph.findNode('A')
        resultZ = graph.findNode('Z')

        #then:
        self.assertEquals(nodeA, resultA)
        self.assertEquals(None, resultZ)
            
    def testPrint(self):
        #given:
        graph = WeightedDigraph()

        #and:
        nodeA = Node('A')
        nodeB = Node('B')
        nodeC = Node('C')
        for node in [nodeA, nodeB, nodeC]: graph.addNode(node)

        #and:
        data = [(nodeA, nodeB, 15, 10), (nodeA, nodeC, 14, 6), (nodeB, nodeC, 3, 1)]
        for source, destination, totalDistance, outdoorDistance in data:
            edge = WeightedEdge(source, destination, totalDistance, outdoorDistance)
            graph.addEdge(edge)

        #expect:
        self.assertEquals('A->B (15.0, 10.0)\nA->C (14.0, 6.0)\nB->C (3.0, 1.0)', str(graph))

    def testFindPaths(self):
        #given:
        graph = load_map('test_complex_map.txt')
        nodeA = filter(lambda node: 'A' == node.name, graph.nodes)[0]
        nodeD = filter(lambda node: 'D' == node.name, graph.nodes)[0]

        #when:
        paths = graph.findPaths(nodeA, nodeD, 20)

        #then:
        self.assertEquals(2, len(paths))
        for path in paths:
            self.assertEquals(nodeA, path.path[0])
            self.assertEquals(nodeD, path.path[-1])

        #and:
        pathStrings = [str(path) for path in paths]
        for pathString in pathStrings:
            self.assertTrue(pathString in ['A->B->E->D', 'A->C->E->D'])

        #and:
        nodeB = filter(lambda node: 'B' == node.name, graph.nodes)[0]
        nodeC = filter(lambda node: 'C' == node.name, graph.nodes)[0]
        pathThroughB = filter(lambda path: nodeB in path.path, paths)[0]
        self.assertEquals(18, pathThroughB.distance)
        self.assertEquals(10, pathThroughB.outerDistance)
        pathThroughC = filter(lambda path: nodeC in path.path, paths)[0]
        self.assertEquals(20, pathThroughC.distance)
        self.assertEquals(9, pathThroughC.outerDistance)

class WeightedPathTest(TestCase):

    def testLink(self):
        #given:
        nodeA = Node('A')
        nodeB = Node('B')
        nodeC = Node('C')

        #and:
        edgeAB = WeightedEdge(nodeA, nodeB, 10, 2)
        edgeBC = WeightedEdge(nodeB, nodeC, 34, 10)

        #and:
        emptyPath = WeightedPath()
        filledPath = WeightedPath([nodeA])

        #when:
        emptyPath.link(edgeAB)
        filledPath.link(edgeAB)
        filledPath.link(edgeBC)

        #then:
        self.assertEquals(2, len(emptyPath.path))
        self.assertEquals(10, emptyPath.distance)
        self.assertEquals(2, emptyPath.outerDistance)

        #and:
        self.assertEquals(3, len(filledPath.path))
        self.assertEquals(44, filledPath.distance)
        self.assertEquals(12, filledPath.outerDistance)

    def testFork(self):
        #given:
        nodeA = Node('A')
        nodeB = Node('B')
        nodeC = Node('C')
        
        #and:
        path = WeightedPath([nodeA, nodeB], 25)
        edge = WeightedEdge(nodeB, nodeC, 9, 2)

        #when:
        forkedPath = path.fork(edge)

        #then:
        self.assertEquals(WeightedPath, type(forkedPath))
        self.assertFalse(path == forkedPath)

        #and:
        self.assertEquals(3, len(forkedPath.path))
        for node in forkedPath.path:
            self.assertTrue(node in [nodeA, nodeB, nodeC])
        self.assertEquals(34, forkedPath.distance)
        self.assertEquals(2, forkedPath.outerDistance)    

if '__main__' == __name__:
    main()
