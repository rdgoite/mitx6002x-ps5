#!/usr/bin/python

from graph import *
from ps5 import *
from unittest import main, TestCase

class Ps5Test(TestCase):

    def testLoadMap(self):
        #given:
        graph = load_map('test_map.txt')

        #expect:
        self.assertEquals(3, len(graph.nodes))
        for node in graph.nodes:
            self.assertTrue(node.name in ['A', 'B', 'C'])
        self.assertEquals(3, len(graph.edges))

        #and:
        nodeA = [node for node in graph.nodes if 'A' == node.name][0]
        self.assertEquals(2, len(graph.edges[nodeA]))
        for edge in graph.edges[nodeA]:
            self.assertTrue(edge.dest.name in ['B', 'C'])
        edgeAB = [edge for edge in graph.edges[nodeA] if 'B' == edge.dest.name][0]
        self.assertEquals(15, edgeAB.totalDistance)
        self.assertEquals(10, edgeAB.outdoorDistance)

    def testBruteForceSearch(self):
        #given:
        graph = load_map('test_complex_map.txt')
        sampleMap = load_map('from_grader.txt')

        #when:
        shortestPath1 = bruteForceSearch(graph, 'A', 'D', 30, 9)
        shortestPath2 = bruteForceSearch(sampleMap, '1', '3', 18, 18)

        #then:
        self.assertEquals('ACED', ''.join(shortestPath1))
        self.assertEquals('123', ''.join(shortestPath2))

if '__main__' == __name__:
    main()
